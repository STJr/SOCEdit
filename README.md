# SRB2 SOC Editor

## License

Copyright (C) Sonic Team Junior, 2015

This code is made available under the terms of the GNU General Public License, version 2. You can see a copy of that [here](http://www.gnu.org/licenses/gpl-2.0.html).
